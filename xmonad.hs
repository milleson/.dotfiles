-- Control
import Control.Arrow (second)
import Control.Applicative
-- Data
import qualified Data.Map        as M
import Data.List(isInfixOf)
import Data.Maybe
import Data.Monoid
import Data.String.Utils(replace)
import Control.Monad
-- System
import System.Directory (setCurrentDirectory)
import System.Exit(exitWith, ExitCode(..))
-- XMonad.Actions
import XMonad
import XMonad.Actions.Commands
import XMonad.Actions.CycleWS
import XMonad.Actions.RotSlaves(rotSlavesDown, rotSlavesUp)
import XMonad.Actions.Search
import XMonad.Actions.ShowText(defaultSTConfig, handleTimerEvent, flashText)
import XMonad.Actions.UpdatePointer
import XMonad.Actions.WindowBringer(bringMenu, gotoMenu)
import XMonad.Actions.WorkspaceNames
-- XMonad.Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.EwmhDesktops hiding (fullscreenEventHook)
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.Place
import XMonad.Hooks.ScreenCorners
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName(setWMName)
import XMonad.Hooks.UrgencyHook
-- XMonad.Layout
import XMonad.Layout
import XMonad.Layout.Accordion
import qualified XMonad.Layout.BoringWindows as B
import XMonad.Layout.DwmStyle
import XMonad.Layout.Fullscreen
import XMonad.Layout.Grid
import XMonad.Layout.LayoutHints
import qualified XMonad.Layout.Magnifier as Mag
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.TrackFloating
import XMonad.Layout.WorkspaceDir
-- XMonad.Prompt
import XMonad.Prompt
import XMonad.Prompt.Shell(shellPrompt)
-- XMonad.StackSet
import qualified XMonad.StackSet as W
-- XMonad.Util
import XMonad.Util.EZConfig
import XMonad.Util.Paste
import XMonad.Util.Run(hPutStrLn, spawnPipe, unsafeSpawn)
import XMonad.Util.Scratchpad(scratchpadSpawnActionCustom, scratchpadManageHook)
import XMonad.Util.WorkspaceCompare(getSortByIndex)
import XMonad.Util.XSelection

-- Custom
import CommandLineClient
import CommandLineServer
import DocksRefresh
import Recompile
import Statusbars
import UrgencyHooks
import Util

myTerminal = "xterm"
myBrowser  = "google-chrome"
myFocusFollowsMouse = True
myBorderWidth = 1
myModMask = mod4Mask
volUp   = "<XF86AudioRaiseVolume>"
volDown = "<XF86AudioLowerVolume>"
volMute = "<XF86AudioMute>"
brightUp   = "<XF86MonBrightnessUp>"
brightDown = "<XF86MonBrightnessDown>"
myTheme = theme statusbarsTheme

myWorkspaces = map show [1..9]
myDefaultWorkspaceNames = zip myWorkspaces $ ["start", "emacs"] ++ map show [3..8] ++ ["deluge"]
defaultNameForWorkspace s = fromMaybe "" $ lookup s myDefaultWorkspaceNames
setCurrentWorkspaceDefaultName  = gets (W.currentTag . windowset) >>=
                                  (setWorkspaceName <*> defaultNameForWorkspace)
setDefaultWorkspaceNames = mapM_ (uncurry setWorkspaceName) myDefaultWorkspaceNames
-- Hide scratchpad workspace (since scratchpad creates a new workspace)
myExcludedWorkspaces = return ["NSP"]

myNormalBorderColor  = "#4e4e4e"
myFocusedBorderColor = "#dddddd"

-- Custom keys
myKeysP :: XConfig l -> [(String, X ())]
myKeysP conf =
    [ ("M-S-<Return>"   , spawn $ XMonad.terminal conf)
    , ("M-p"            , spawn "dmenu_run")
    , ("M-S-p"          , gotoMenu)
    , ("M-C-p"          , bringMenu)
    , ("M-S-c"          , kill)
    , ("M-C-c"          , withFocused $ \w -> spawn ("xkill -id " ++ show w))
      -- Changed XMonad.Layout to handle PrevLayout and LastLayout
    , ("M-S-<Space>"    , sendMessage PrevLayout)
    , ("M-<Space>"      , sendMessage NextLayout)
    , ("M-n"            , refresh)
    , ("M-<Tab>"        , windows W.focusDown)
    , ("M-j"            , windows W.focusDown)
    , ("M-k"            , windows W.focusUp)
    , ("M-m"            , windows W.focusMaster)
    , ("M-<Return>"     , windows W.swapMaster)
    , ("M-S-j"          , windows W.swapDown)
    , ("M-S-k"          , windows W.swapUp)
    , ("M-C-j"          , rotSlavesDown)
    , ("M-C-k"          , rotSlavesUp)
    , ("M-h"            , sendMessage Shrink)
    , ("M-l"            , sendMessage Expand)
    , ("M-S-h"          , sendMessage MirrorShrink)
    , ("M-S-l"          , sendMessage MirrorExpand)
    , ("M-t"            , withFocused $ windows . W.sink)
    , ("M-,"            , sendMessage (IncMasterN 1))
    , ("M-."            , sendMessage (IncMasterN (-1)))
    , ("M-S-q"          , io (exitWith ExitSuccess))
    , ("M-b"            , sendMessage $ ToggleStruts)
    , ("M-q"            , recompileXmonadC $ recompileConfig)
    , ("M-a"            , sendKey noModMask xK_Left)
    , ("M-d"            , sendKey noModMask xK_Right)
    , (volUp            , spawn $ "amixer --card=0 set Master 2+;" ++ 
                                  myHome ++ "/.xmobar/volume | xargs volnoti-show")
    , (volDown          , spawn $ "amixer --card=0 set Master 2-;" ++ 
                                  myHome ++ "/.xmobar/volume | xargs volnoti-show")
    , (volMute          , spawn $ "amixer --card=0 set Master toggle;" ++
                                 myHome ++ "/.xmobar/muted")
    , (brightUp         , spawn $ myHome ++ "/.xmobar/light")
    , (brightDown       , spawn $ myHome ++ "/.xmobar/light")
    , ("<Print>"        , spawn $ "scrot -e 'mv $f " ++ myHome ++ "/screenshots/'")
    , ("M-C-<R>"        , shiftNextView Next)
    , ("M-S-<R>"        , shiftNextView Next >> shiftAndView Next)
    , ("M-C-<L>"        , shiftNextView Prev)
    , ("M-S-<L>"        , shiftNextView Prev >> shiftAndView Prev)
    , ("M-<L>"          , shiftAndView Prev)
    , ("M-<R>"          , shiftAndView Next)
    -- Toggle fullscreen layout
    , ("M-S-<Tab>"      , sendMessage $ Toggle FULL)
    , ("M-<F1>"         , spawn "google-chrome")
    , ("M-<F2>"         , spawn "/chromium/src/out/Release/chrome")
    , ("M-<F3>"         , spawn "emacs")
    , ("M-<F8>"        , spawn "setxkbmap -layout se -variant mac")
    , ("M-<F9>"        , spawn "setxkbmap -v workman-p && xset r 66")
    , ("M-<F10>"        , spawn "sudo netcfg down home; sudo netcfg home &")
    , ("M-<F11>"        , spawn "lxtask")
    , ("M-<F12>"        , spawn $ "emacs " ++ myHome ++ "/.dotfiles/xmonad-lib/xmonad.hs")
    , ("M-S-w"          , sendKey controlMask xK_Home)
    , ("M-S-s"          , sendKey controlMask xK_End)
    -- Map Home to A, End to D, Up to w, Down to s
    , ("M-S-a"          , sendKey noModMask xK_Home)
    , ("M-S-d"          , sendKey noModMask xK_End)
    , ("M-w"            , sendKey noModMask xK_Up)
    , ("M-s"            , sendKey noModMask xK_Down)
    -- Open file explorer in scratchpad
    , ("M-x"            , scratchpadSpawnActionCustom "pcmanfm --name=scratchpad")
    -- Reset current directory to $HOME
    , ("M-C-x"          , catchIO $ setCurrentDirectory myHome)
    -- Open shell prompt
    , ("M-o"            , shellPrompt defaultXPConfig)
    -- Search google on selected text from anywhere and jump to browser workspace
    , ("M-C-<Return>"   , join $ liftM (search myBrowser (getSite google)) getSelection)
    -- , ("M-S-x"          , putToStatusbar "Print text in title-bar")
    ]
    where -- Shift view and stay in current workspace (skip scratchpad workspace)
          shiftAndView dir = findWorkspace getSortByIndex dir (WSIs notNSP) 1 >>=
                             windows . W.greedyView
          -- Shift view and move to next workspace (skip scratchpad workspace)
          shiftNextView dir = findWorkspace getSortByIndex dir (WSIs notNSP) 1 >>=
                              windows . W.shift
          notNSP = return $ ("NSP" /=) . W.tag
          getSite (SearchEngine _ site) = site

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    [((m .|. modm, k), f i)
     | (i, k) <- zip (workspaces conf) [xK_exclam, xK_at, xK_numbersign, xK_dollar, xK_percent, xK_asciicircum, xK_ampersand, xK_asterisk, xK_parenleft, xK_parenright]
     , (f, m) <- [(windows . W.greedyView , 0),
                  (windows . W.shift      , controlMask),
                  (windows . W.shift >> windows . W.greedyView, shiftMask)]
    ]

leftMouseButton = button1
rightMouseButton = button3

myMouseBindings (XConfig {XMonad.modMask = modm}) 
    = M.fromList $
      [((modm, leftMouseButton), (\w -> focus w
                                        >> mouseMoveWindow w >> windows W.shiftMaster))
      ,((modm, rightMouseButton), (\w -> focus w
                                         >> mouseResizeWindow w >> windows W.shiftMaster))
    ,((modm.|. controlMask , button1),
      -- Copy the class name of the window clicked (ex: Wine when clicking teamviewer)
      \w -> focus w >> spawn ("xprop -id " ++ show w ++
                              " | grep -i class" ++
                              " | awk -vFS=$'\n' -vRS='' -vORS='' -F '\"' '{print $4}'" ++
                              " | xclip -selection c") >>
             -- Bellow code compile but don't work (causes xmonad to crash).
             -- (io $ readProcess "cat" ["~/xprop.log"] "") >>=
            flashText defaultSTConfig 1 "className -> clipboard")
    ]

myLayouts = myLayoutModifiers $ resizable ||| tiled ratio ||| bigTop ||| tabs
  where -- Fullscreen layout
        -- full      = Full
        -- Resizable 'normal' layout
        resizable = renamed [Replace "Normal"] $ ResizableTall nmaster delta ratio []
        -- Tiled, current frame magnified (master on the left)
        tiled r   = renamed [Replace "Magnify"] . Mag.magnifiercz' 1.4 $ ResizableTall nmaster delta r []
        -- Same as above but master on the top
        bigTop    = renamed [Replace "M-Magnify"] . Mirror . tiled $ 83/100
        -- Tabbed layout (each window is a tab
        tabs      = renamed [CutWordsRight 1] . noBorders . trackFloating $ tabbed shrinkText myTheme

        nmaster = 1
        ratio   = 14/25
        delta   = 3/100

myLayoutModifiers
    = -- Makes the fullscreened window fill the entire screen only if it is currently focused.
      fullscreenFocus
      -- Adjust layout automagically: don't cover up any docks, status bars, etc.
    . avoidStruts
      -- No borders on myAmbiguity list
    . lessBorders myAmbiguity
    . B.boringWindows
    . mkToggle (NOBORDERS ?? FULL ?? MIRROR ?? EOT)
    . trackFloating
    . workspaceDir "~"
    . renamed [CutWordsLeft 1] . dwmStyle shrinkText myTheme

instance Monoid Ambiguity where
    mappend = Combine Union
    mempty = XMonad.Layout.NoBorders.Never

-- Provide a list of windows that should not have borders
myAmbiguity = Screen <+> EmptyScreen <+> OnlyFloat <+> OtherIndicated

-- Hooks for window/dialogs floating.
myManageHooks = transience' <+> composeOne
    [ composeAll $ map mkAppHook myAppHooks
    , isDialog -?> doCenterFloat
    , Just <$> placeHook (smart (0.5, 0.5))
    ]
    where mkAppHook (a,h) = className =? a -?> h

myAppHooks = [ ("Wine"    , unfloat) -- Teamviewer
             , ("Deluge"  , doShift "9")
             , ("Firefox" , firefoxHook)
             , ("Dotplot.tcl" , doFloat)
             ]
    where unfloat = ask >>= doF . W.sink

-- Firefox hook (make dialogs (ex: downloads) floating (i.e. not tiled)).
firefoxHook = composeOne
    [ isInDialogs <||> isDialog -?> doCenterFloat]
    where isInDialogs = (`elem` ["Browser", "Download", "Places", "DTA", "Toplevel"]) <$> appName

manageScratchPad :: ManageHook
manageScratchPad = scratchpadManageHook (W.RationalRect l t w h)
 where h = 0.8
       w = 0.9
       t = 0.1
       l = 0.05

myStartupHook = do runDaemons
                   setDefaultWorkspaceNames
    
runDaemons = mapM_ (spawn . ("exec "++))
    [ "caffeine"
    , "volnoti"
    , "sudo netcfg home"
    ]

-- Events for screen corners (move to next non empty workspace)
-- Seems to be a bit buggy, sometimes some windows don't register the event.
myScreenCorners = []
 --    [ (SCLowerRight, moveTo Next myCycler)
 --    , (SCLowerLeft,  moveTo Prev myCycler)
 --    ]
 -- where myCycler = WSIs $ do
 --                    isExc <- isExcluded
 --                    isHid <- isHidden
 --                    return $ isJust . W.stack <&&> isHid <&&> isExc
 --       isExcluded = do 
 --         exc <- myExcludedWorkspaces
 --         return $ (`notElem` exc) . W.tag
 --       isHidden = do
 --         hs <- gets $ map W.tag . W.hidden . windowset
 --         return $ (`elem` hs) . W.tag

-- wmname LG3D - Which fixes some gtk applications (matlab, so on..).
wmName conf = conf { startupHook = startupHook conf >> setWMName "LG3D" }

myConfig = mutators $ defaultConfig {
               terminal           = myTerminal,
               focusFollowsMouse  = myFocusFollowsMouse,
               borderWidth        = myBorderWidth,
               modMask            = myModMask,
               workspaces         = myWorkspaces,
               normalBorderColor  = myNormalBorderColor,
               focusedBorderColor = myFocusedBorderColor,
               keys               = myKeys
                                    -- Emacs-like keybinding (M-S/C-*).
                                    <+> (mkKeymap <*> myKeysP),
               mouseBindings      = myMouseBindings,
               layoutHook         = myLayouts,
               manageHook         = manageDocks
                                    <+> myManageHooks
                                    <+> manageScratchPad,
               handleEventHook    = fullscreenEventHook
                                    <+> handleTimerEvent
                                    -- Whenever a new dock appears, refresh the layout.
                                    <+> docksEventHook
                                    <+> docksRefreshEventHook
                                    <+> hintsEventHook
                                    <+> screenCornerEventHook
                                    <+> commandLineEventHook myCommandlineTable,
               startupHook        = myStartupHook
                                    <+> statusbarsStartupHook' myExcludedWorkspaces
                                    <+> startupHook defaultConfig
                                    <+> addScreenCorners myScreenCorners,
               logHook            = statusbarsLogHook
             }
    where mutators = withMyUrgencyHook . wmName . ewmh
          withMyUrgencyHook = withUrgencyHookC (gtkUrgencyHook <+> borderUrgencyHook "#FF0000")
                              urgencyConfig { suppressWhen = Focused }

-- sendCommandsAtStartup allows xmonad to be called with
-- with arguments, i.e. xmonad -c nextLayout. Need to rebuild
-- the xmonad package to enable arguments.
main = sendCommandsAtStartup >> xmonad myConfig
